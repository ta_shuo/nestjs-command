import { AppModule } from './app/app.module'
import { execModuleTest } from './utils/exec-module-test'

describe('module-test', () => {
  it('should return result', async () => {
    const result = await execModuleTest(AppModule, 'add', {})
    expect(result).toBe('add')
    const helloResult = await execModuleTest(AppModule, 'testMultiple:hello', {});
    expect(helloResult).toBe('hello');
    const hello2Result = await execModuleTest(AppModule, 'testMultiple:hello2', {});
    expect(hello2Result).toBe('hello2');
    const hello3Result = await execModuleTest(AppModule, 'testMultiple:hello3', {});
    expect(hello3Result).toBe('hello3');
    const originTestResult = await execModuleTest(AppModule, 'originTest', {});
    expect(originTestResult).toBe('originTest');
    const originTest2Result = await execModuleTest(AppModule, 'originTest2', {});
    expect(originTest2Result).toBe('originTest2');
  })
})
