import { Injectable } from "@nestjs/common";
import * as yargs from "yargs";
import { OriginYargsCommand } from "../../lib/command.decorator";

@Injectable()
@OriginYargsCommand()
export class TestService implements yargs.CommandModule {
    command = "originTest";
    describe = "origin command test";
    test: string;

    builder(args: yargs.Argv) {
        return args.option("test", {
            alias: "t",
            type: "boolean",
            describe: "test param.",
            default: false,
        });
    }

    async handler(args: yargs.Arguments) {
        return new Promise<string>((resolve) => setTimeout(() => resolve('originTest'), 0));
    }
}

export class Test2Service implements yargs.CommandModule {
    command = "originTest2";
    describe = "origin command test";

    builder(args: yargs.Argv) {
        return args.option("test", {
            alias: "t",
            type: "boolean",
            describe: "test param.",
            default: false,
        });
    }

    async handler(args: yargs.Arguments) {
        return new Promise<string>((resolve) => setTimeout(() => resolve('originTest2'), 0));
    }
}

@Injectable()
@OriginYargsCommand()
export class OriginCommandService extends Test2Service {}

