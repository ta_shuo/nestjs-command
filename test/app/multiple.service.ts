import { Injectable } from '@nestjs/common';
import { Commands, Command } from '../../lib/command.decorator';

@Commands({
    command: 'testMultiple',
    describe: 'test',
})
@Injectable()
export class MultipleService {
  test: number;

  async hello(name: string) {
    return new Promise<string>((resolve) => setTimeout(() => resolve('hello'), 0))
  }

  async hello2(name: string) {
    return new Promise<string>((resolve) => setTimeout(() => resolve('hello2'), 0))
  }

  async hello3(name: string) {
    return new Promise<string>((resolve) => setTimeout(() => resolve('hello3'), 0))
  }
}
