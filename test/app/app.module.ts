import { Module } from '@nestjs/common';
import { CommandModule } from '../../lib/command.module';
import { AppService } from './app.service';
import { MultipleService } from './multiple.service';
import { OriginCommandService, TestService } from './originCommand.service';

@Module({
  imports: [CommandModule],
  providers: [AppService, MultipleService, TestService, OriginCommandService]
})
export class AppModule {}
