import 'reflect-metadata';
import { SetMetadata } from '@nestjs/common';
import { PositionalOptions, Options } from 'yargs';

export const COMMAND_HANDLER_METADATA = '__command-handler-metadata__';
export const COMMAND_ARGS_METADATA = '__command-args-metadata__';
export const COMMANDS_HANDLER_METADATA = '__commands-handler-metadata__';
export const ORIGIN_COMMAND_HANDLER_METADATA = '__origin-command-handler-metadata__';
export enum CommandParamTypes {
  POSITIONAL = 'POSITIONAL',
  OPTION = 'OPTION',
  ARGV = 'ARGV'
}

export type CommandParamMetadata<O> = {
  [type in CommandParamTypes]: CommandParamMetadataItem<O>[]
};
export interface CommandParamMetadataItem<O> {
  index: number;
  option: O;
}
const createCommandParamDecorator = <O>(paramtype: CommandParamTypes) => {
  return (option?: O): ParameterDecorator => (target, key, index) => {
    const params =
      Reflect.getMetadata(COMMAND_ARGS_METADATA, target[key]) || {};
    Reflect.defineMetadata(
      COMMAND_ARGS_METADATA,
      {
        ...params,
        [paramtype]: [...(params[paramtype] || []), { index, option }]
      },
      target[key]
    );
  };
};

export interface CommandMetadata {
  params: CommandParamMetadata<CommandPositionalOption | CommandOptionsOption>;
  option: CommandOption;
}
export interface CommandOption {
  aliases?: string[] | string;
  command: string[] | string;
  describe?: string | false;
}
export function Command(option: CommandOption): MethodDecorator {
  return (
    target: object,
    key: string | symbol,
    descriptor: PropertyDescriptor
  ) => {
    const metadata: CommandMetadata = {
      params: Reflect.getMetadata(COMMAND_ARGS_METADATA, descriptor.value),
      option
    };

    // todo 根据design:paramtypes自动补充默认的参数
    // console.log(target, key);
    // console.log(Reflect.getMetadata('design:type', target, key));
    // console.log(Reflect.getMetadata('design:paramtypes', target, key));
    // console.log(Reflect.getMetadata('design:paramtypes', target, key).map((p) => console.log(p.name)));

    SetMetadata(COMMAND_HANDLER_METADATA, metadata)(target, key, descriptor);
  };
}

export const Commands = (options: CommandOption): ClassDecorator => (target: any): void => Reflect.defineMetadata(COMMANDS_HANDLER_METADATA, options, target);

export const OriginYargsCommand = (): ClassDecorator => (target: any): void => {
  // console.log('OriginYargsCommand');
  Reflect.defineMetadata(ORIGIN_COMMAND_HANDLER_METADATA, true, target);
}

export interface CommandPositionalOption extends PositionalOptions {
  name: string;
}
export const Positional = createCommandParamDecorator<CommandPositionalOption>(
  CommandParamTypes.POSITIONAL
);

export interface CommandOptionsOption extends Options {
  name: string;
}
export const Option = createCommandParamDecorator<CommandOptionsOption>(
  CommandParamTypes.OPTION
);

export const Argv = createCommandParamDecorator(CommandParamTypes.ARGV);
