import { Injectable } from '@nestjs/common';
import { Command, Commands } from './command.decorator';

@Commands({
    command: 'testMultiple',
    describe: 'test',
})
@Injectable()
export class Multiple2Service {
  protected test: number;

  async hello(name?: string) {
    return new Promise<string>((resolve) => setTimeout(() => resolve(`hello-${name}`), 0))
  }

  @Command({
    command: 'add',
    describe: 'add',
  })
  async add(p1: string, p2?: number, p3 = 'test') {
    return new Promise<string>((resolve) => setTimeout(() => resolve('add'), 0))
  }
}
