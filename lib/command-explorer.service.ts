import compact from 'lodash.compact';
import flattenDeep from 'lodash.flattendeep';
import { CommandModule, Argv, Arguments } from 'yargs';
import { Injectable } from '@nestjs/common';
import { MetadataScanner, ModulesContainer } from '@nestjs/core';
import { Injectable as IInjectable } from '@nestjs/common/interfaces';
import {
  COMMAND_HANDLER_METADATA,
  CommandMetadata,
  CommandParamTypes,
  CommandParamMetadata,
  CommandOptionsOption,
  CommandPositionalOption,
  CommandParamMetadataItem,
  COMMANDS_HANDLER_METADATA,
  Command,
  ORIGIN_COMMAND_HANDLER_METADATA
} from './command.decorator';
import { CommandService } from './command.service';

@Injectable()
export class CommandExplorerService {
  constructor(
    private readonly modulesContainer: ModulesContainer,
    private readonly metadataScanner: MetadataScanner,
    private readonly commandService: CommandService
  ) {}

  explore(): CommandModule[] {
    const components = [...this.modulesContainer.values()].map(
      module => module.providers
    );
    return compact(
      flattenDeep<CommandModule>(
        components.map(component =>
          [...component.values()].map(({ instance, token }) =>
            this.filterCommands(instance, token)
          )
        )
      )
    );
  }

  protected filterCommands(instance: IInjectable, token: any): CommandModule[] {
    if (!instance) return;
    const prototype = Object.getPrototypeOf(instance);

    // originYargsCommand
    const originYargsCommandMeta = Reflect.getMetadata(ORIGIN_COMMAND_HANDLER_METADATA, instance.constructor);
    if (originYargsCommandMeta) {
      let builder, handler = null;
      let currentProto = prototype;
      // 遍历原型链获取builder和handler
      while (!builder && !handler && currentProto) {
        if (!builder && Object.getOwnPropertyDescriptor(currentProto, 'builder')) {
          builder = Object.getOwnPropertyDescriptor(currentProto, 'builder').value;
        }
        if (!handler && Object.getOwnPropertyDescriptor(currentProto, 'handler')) {
          handler = Object.getOwnPropertyDescriptor(currentProto, 'handler').value;
        }
        currentProto = Object.getPrototypeOf(currentProto);
      }
      if (!handler) {
        return;
      }
      return [{
        command: (instance as any)?.command,
        describe: (instance as any)?.describe,
        builder: builder,
        handler: handler,
      }];
    }

    // multiCommands
    const commandsMeta = Reflect.getMetadata(COMMANDS_HANDLER_METADATA, instance.constructor);
    if (commandsMeta) {
      const commands = Object.getOwnPropertyNames(prototype).filter((f) => !Reflect.hasMetadata(COMMAND_HANDLER_METADATA, prototype[f]) && f !== 'constructor');
      commands.map((f) => {
        Command({command: `${commandsMeta.command}:${f}`, describe: commandsMeta.describe ? `${commandsMeta.describe}:${f}` : ''})(prototype, f, Object.getOwnPropertyDescriptor(prototype, f))
      });
    }

    const components = this.metadataScanner.scanFromPrototype(
      instance,
      prototype,
      name => this.extractMetadata(instance, prototype, name)
    );
    return components
      .filter(command => !!command.metadata)
      .map<CommandModule>(command => {
        const exec = instance[command.methodName].bind(instance);

        const builder: NonNullable<CommandModule['builder']> = (yargs) =>
          this.generateCommandBuilder(command.metadata.params, yargs);

        const handler: NonNullable<CommandModule['handler']> = async (args) => {
          const params = this.generateCommandHandlerParams(
            command.metadata.params,
            args
          );
          const result = await exec(...params);
          return result;
        };

        return {
          ...command.metadata.option,
          builder,
          handler,
        };
      });
  }

  protected extractMetadata(instance, prototype, methodName: string) {
    const callback = prototype[methodName];
    const metadata: CommandMetadata = Reflect.getMetadata(
      COMMAND_HANDLER_METADATA,
      callback
    );

    return {
      methodName,
      metadata
    };
  }

  protected iteratorParamMetadata<O>(
    params: CommandParamMetadata<O>,
    callback: (item: CommandParamMetadataItem<O>, key: string) => void
  ) {
    if (!params) {
      return;
    }

    Object.keys(params).forEach(key => {
      const param: CommandParamMetadataItem<O>[] = params[key];
      if (!param || !Array.isArray(param)) {
        return;
      }

      param.forEach(metadata => callback(metadata, key));
    });
  }

  private generateCommandHandlerParams(
    params: CommandParamMetadata<
      CommandOptionsOption | CommandPositionalOption
    >,
    argv: Arguments
  ) {
    const list = [];

    this.iteratorParamMetadata(params, (item, key) => {
      switch (key) {
        case CommandParamTypes.OPTION:
          list[item.index] = argv[(item.option as CommandOptionsOption).name];
          break;

        case CommandParamTypes.POSITIONAL:
          list[item.index] =
            argv[(item.option as CommandPositionalOption).name];
          break;

        case CommandParamTypes.ARGV:
          list[item.index] = argv;

        default:
          break;
      }
    });

    return list;
  }

  private generateCommandBuilder(
    params: CommandParamMetadata<
      CommandOptionsOption | CommandPositionalOption
    >,
    yargs: Argv
  ) {
    this.iteratorParamMetadata(params, (item, key) => {
      switch (key) {
        case CommandParamTypes.OPTION:
          yargs.option(
            (item.option as CommandOptionsOption).name,
            item.option as CommandOptionsOption
          );
          break;

        case CommandParamTypes.POSITIONAL:
          yargs.positional(
            (item.option as CommandPositionalOption).name,
            item.option as CommandPositionalOption
          );
          break;

        default:
          break;
      }
    });

    return yargs;
  }
}
